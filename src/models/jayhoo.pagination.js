jayhoo.Pagination = function() {
    this.resetEvent = new jayhoo.Event(this);
    this.pageChangedEvent = new jayhoo.Event(this);
    this.paginationSize = 0;
    this.totalItems = 0;
    this.lastPage = 0;
    this.currentPage = 0;
}
jayhoo.Pagination.prototype = {
    reset: function(paginationSize, totalItems) {
        this.totalItems = totalItems || 0;
        this.paginationSize = paginationSize || 0;
        if (this.paginationSize) {
            this.totalPages = Math.ceil(this.totalItems / this.paginationSize);
        } else {
            this.totalPages = 0;
        }
        this.resetEvent.notify({
            paginationSize: this.paginationSize,
            totalPages:     this.totalPages,
            totalItems:     this.totalItems
        });
    },
    setCurrentPage: function(page) {
        page = page || 0;
        if (typeof page == 'string') {
            if (page == 'first') page = 1;
            if (page == 'previous') page = this.currentPage - 1;
            if (page == 'next') page = this.currentPage + 1;
            if (page == 'last') page = this.lastPage;
        } else if (typeof page != 'number') {
            page = parseInt(page);
        }
        if (!isNaN(page) && page > 0 && page <= this.totalPages) {
            var startOffset = (page - 1) * this.paginationSize;
            var endOffset = startOffset + this.paginationSize - 1;
            if (endOffset >= this.totalItems) {
                endOffset = this.totalItems - 1;
            }
            this.currentPage = page;
            this.pageChangedEvent.notify({
                currentPage: this.currentPage,
                startOffset: startOffset,
                endOffset:   endOffset
            });
        }
        if (this.totalPages == 0 && this.totalItems == 0) {
            this.pageChangedEvent.notify({
                currentPage: this.currentPage,
                startOffset: 0,
                endOffset:   0
            });
        }
    },
    goToPreviousPage: function() {
        this.setCurrentPage(this.currentPage - 1);
    },
    goToNextPage: function() {
        this.setCurrentPage(this.currentPage + 1);
    },
    goToFirstPage: function() {
        this.setCurrentPage(1);
    },
    goToLastPage: function() {
        this.setCurrentPage(this.totalItems);
    }
};
