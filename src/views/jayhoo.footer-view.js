jayhoo.FooterView = function() {

    this.cleanOrphanedDataClickedEvent = new jayhoo.Event(this);
    this.outerLinksClickedEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.footer');
    this.setStrings();

    var helpWindow = null, self = this;
    this._containerElement.addEventListener('click', function(e) {
        e.preventDefault();
        if (e.target.nodeName == 'A') {
            switch (e.target.className) {
                case 'clean-data':
                    self.cleanOrphanedDataClickedEvent.notify();
                    break;
                case 'view-help':
                    if (helpWindow) {
                        helpWindow.focus();
                    } else {
                        helpWindow = window.open(e.target.href, 'jayhoo', 'width=500,height=500,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=1');
                        helpWindow.onunload = function() {
                            helpWindow = null;
                        };
                    }
                    break;
                default:
                    self.outerLinksClickedEvent.notify({
                        url: e.target.href
                    });
                    break;
            }
        }
    });
};
jayhoo.FooterView.prototype = {
    setStrings: function() {
        this._containerElement.querySelector('.view-help').innerHTML = _('View Help &amp; Documentation');
        this._containerElement.querySelector('.clean-data').innerHTML = _('Clean Data Files');
    }
};
