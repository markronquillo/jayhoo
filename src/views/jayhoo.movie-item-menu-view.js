jayhoo.MovieItemMenuView = function() {

    this.menuItemClickedEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.movie-item-menu');
    this._currentMovieId = null;

    this.setStrings();

    var self = this;
    this._containerElement.addEventListener('mouseleave', function() {
        self.close();
    });
    this._containerElement.addEventListener('click', function(e) {
        if (e.target.nodeName == 'LI') {
            e.preventDefault();
            self.menuItemClickedEvent.notify({
                movieId: self._currentMovieId,
                menu: e.target.className
            });
            self.close();
        }
    });
};
jayhoo.MovieItemMenuView.prototype = {
    show: function(movieId, top, left) {
        this._currentMovieId = movieId;
        this._containerElement.style.display = 'block';
        this._containerElement.style.top = top + 'px';
        this._containerElement.style.left = left + 'px';
    },
    close: function() {
        this._containerElement.style.display = 'none';
    },
    setStrings: function() {
        this._containerElement.querySelector('.movie-item-menu-more-info').innerHTML = _('more info');
        this._containerElement.querySelector('.movie-item-menu-web-info').innerHTML = _('web info');
        this._containerElement.querySelector('.movie-item-menu-update-info').innerHTML = _('update info');
        this._containerElement.querySelector('.movie-item-menu-update-poster').innerHTML = _('update poster image');
        this._containerElement.querySelector('.movie-item-menu-open-folder').innerHTML = _('open folder location');
    }
};
