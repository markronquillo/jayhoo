jayhoo.LightBoxView = function() {

    this.onShowPreviousMovieEvent = new jayhoo.Event(this);
    this.onShowNextMovieEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.lightbox');
    this._posterElement = this._containerElement.querySelector('.movie-poster');

    this._plotElement = this._containerElement.querySelector('.movie-plot');
    this._titleElement = this._containerElement.querySelector('.movie-title');
    this._actorsElement = this._containerElement.querySelector('.movie-actors');
    this._directorsElement = this._containerElement.querySelector('.movie-directors');
    this._ratedElement = this._containerElement.querySelector('.movie-rated');
    this._genresElement = this._containerElement.querySelector('.movie-genres');
    this._releasedElement = this._containerElement.querySelector('.movie-released');
    this._runtimeElement = this._containerElement.querySelector('.movie-runtime');
    this._writersElement = this._containerElement.querySelector('.movie-writers');
    this._ratingElement = this._containerElement.querySelector('.movie-rating');
    this._fileNameElement = this._containerElement.querySelector('.movie-file-name');
    this._fileSizeElement = this._containerElement.querySelector('.movie-file-size');
    this._fileFolderElement = this._containerElement.querySelector('.movie-file-folder');
    this._fileDownloadedElement = this._containerElement.querySelector('.movie-file-downloaded');

    this._currentMovieId = null;

    this.setStrings();

    var self = this;
    this._containerElement.querySelector('.close').addEventListener('click', function(e) {
        e.preventDefault();
        self.hide();
    });
    this._containerElement.querySelector('.previous').addEventListener('click', function(e) {
        e.preventDefault();
        self.onShowPreviousMovieEvent.notify({ movieId: self._currentMovieId });
    });
    this._containerElement.querySelector('.next').addEventListener('click', function(e) {
        e.preventDefault();
        self.onShowNextMovieEvent.notify({ movieId: self._currentMovieId });
    });
    window.addEventListener('keyup', function(e) {
        if (self._containerElement.style.display == 'block') {
            switch (e.keyCode) {
                case 27:
                    self.hide();
                    break;
                case 37:
                    self.onShowPreviousMovieEvent.notify({ movieId: self._currentMovieId });
                    break;
                case 39:
                    self.onShowNextMovieEvent.notify({ movieId: self._currentMovieId });
                    break;
            }
        }
    });
};

jayhoo.LightBoxView.prototype = {
    show: function(movie) {
        this.clear();

        this._titleElement.innerHTML = movie.info.title + ' (' + movie.info.year + ')';

        if (movie.info.poster) {
            this._posterElement.style.backgroundImage = 'url("' + movie.info.poster + '")';
        }
        if (movie.info.externalId) {
            this._plotElement.innerHTML = movie.info.plot;
            this._actorsElement.innerHTML = movie.info.actors;
            this._directorsElement.innerHTML = movie.info.directors;
            this._ratedElement.innerHTML = movie.info.rated;
            this._genresElement.innerHTML = movie.info.genres;
            this._releasedElement.innerHTML = movie.info.released.toDateString();
            this._runtimeElement.innerHTML = movie.info.runtime;
            this._writersElement.innerHTML = movie.info.writers;
            new jayhoo.RatingView(this._ratingElement, movie.info);
        }

        this._fileNameElement.innerHTML = movie.file.name + '.' + movie.file.extension;
        this._fileSizeElement.innerHTML = formatBytes(movie.file.size);
        this._fileFolderElement.innerHTML = movie.file.folder;
        this._fileDownloadedElement.innerHTML = movie.file.created.toDateString();

        this._containerElement.style.display = 'block';
        this._currentMovieId = movie.id;

        document.body.className = 'no-scroll';
    },
    hide: function() {
        this._containerElement.style.display = 'none';
        document.body.className = '';
    },
    clear: function() {
        this._posterElement.style.backgroundImage = 'url(res/camera.png)';
        this._plotElement.innerHTML = '&mdash;';
        this._titleElement.innerHTML = '&mdash;';
        this._actorsElement.innerHTML = '&mdash;';
        this._directorsElement.innerHTML = '&mdash;';
        this._ratedElement.innerHTML = '&mdash;';
        this._genresElement.innerHTML = '&mdash;';
        this._releasedElement.innerHTML = '&mdash;';
        this._runtimeElement.innerHTML = '&mdash;';
        this._writersElement.innerHTML = '&mdash;';
    },
    setStrings: function() {
        this._containerElement.querySelectorAll('.movie-details-header')[0].innerHTML = _('Movie Information');
        this._containerElement.querySelectorAll('.movie-details-header')[1].innerHTML = _('File Information');
        this._containerElement.querySelector('.label-movie-actors').innerHTML = _('Actor(s)');
        this._containerElement.querySelector('.label-movie-directors').innerHTML = _('Director(s)');
        this._containerElement.querySelector('.label-movie-rated').innerHTML = _('Rated');
        this._containerElement.querySelector('.label-movie-genres').innerHTML = _('Genre(s)');
        this._containerElement.querySelector('.label-movie-released').innerHTML = _('Released');
        this._containerElement.querySelector('.label-movie-runtime').innerHTML = _('Runtime');
        this._containerElement.querySelector('.label-movie-writers').innerHTML = _('Writer(s)');
        this._containerElement.querySelector('.label-movie-rating').innerHTML = _('Rating');
        this._containerElement.querySelector('.label-movie-file-name').innerHTML = _('Filename');
        this._containerElement.querySelector('.label-movie-file-size').innerHTML = _('File Size');
        this._containerElement.querySelector('.label-movie-file-folder').innerHTML = _('File Folder');
        this._containerElement.querySelector('.label-movie-file-downloaded').innerHTML = _('File Downloaded');
        this._containerElement.querySelector('.close').title = _('Press Escape to Close');
    }
};
