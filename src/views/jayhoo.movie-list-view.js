jayhoo.MovieListView = function() {

    this._movieItemMenu = new jayhoo.MovieItemMenuView();

    this.movieItemMenuClickedEvent = this._movieItemMenu.menuItemClickedEvent;
    this.playMovieClickedEvent = new jayhoo.Event(this);
    this.moreOptionsClickedEvent = new jayhoo.Event(this);

    this._containerElement = document.querySelector('.movie-list');
    this._movieItemTemplate = document.querySelector('#movie-item-template').innerHTML;

    var self = this;
    this._containerElement.addEventListener('click', function(e) {
        e.preventDefault();
        if (e.target.nodeName == 'A') {
            var movieId = e.target.parentNode.parentNode.parentNode.parentNode.id;
            switch (e.target.className) {
                case 'movie-item-play':
                    self.playMovieClickedEvent.notify({
                        movieId: movieId
                    });
                    break;
                case 'movie-item-more-options':
                    self._movieItemMenu.show(movieId, e.target.offsetTop, e.target.offsetLeft);
                    break;
            }
        } else if (e.target.nodeName == 'IMG') {
            self.movieItemMenuClickedEvent.notify({
                movieId: e.target.parentNode.parentNode.id,
                menu: 'movie-item-menu-more-info'
            });
        } else if (e.target.className.indexOf('movie-poster') != -1) {
            self.movieItemMenuClickedEvent.notify({
                movieId: e.target.parentNode.id,
                menu: 'movie-item-menu-more-info'
            });
        }
    });
};

jayhoo.MovieListView.prototype = {
    beginAddMovies: function() {
        this._containerElement.innerHTML = '';
        this._documentFragment = document.createDocumentFragment();
    },
    endAddMovies: function() {
        this._containerElement.appendChild(this._documentFragment);
        this._containerElement.scrollTop = 0;
        this._documentFragment = null;
    },
    addMovie: function(movie, currentMenu) {
        var div = document.createElement('div');
        div.innerHTML = this._movieItemTemplate;

        var li = div.querySelector('li');
        li.id = movie.id;
        this._documentFragment.appendChild(li);

        this.updateMovieListItem(li, movie);
        this.addMovieListItemOtherInfo(li, movie, currentMenu);
    },
    updateMovieListItem: function(li, movie) {

        if (!movie) {
            movie = li;
            li = this._containerElement.querySelector('#' + movie.id);
        }
        if (!li && this._documentFragment) {
            li = this._documentFragment.querySelector('#' + movie.id);
        }
        if (!li) {
            return;
        }

        var imageContainer = li.querySelector('.movie-poster');
        imageContainer.className = 'movie-poster default';

        li.querySelector('.movie-main-caption').innerHTML = movie.info.title + ' (' + movie.info.year + ')';

        imageContainer.className = 'movie-poster loader';
        var img = li.querySelector('img');
        img.style.display = 'none';
        img.onload = function() {
            this.style.display = 'inline-block';
            imageContainer.className = 'movie-poster';
        };
        img.onerror = function() {
            imageContainer.className = 'movie-poster';
        };
        if (movie.info.poster) {
            img.src = movie.info.poster.replace('SX300', 'SX150');
        } else {
            imageContainer.className = 'movie-poster default';
        }
    },
    addMovieListItemOtherInfo: function(li, movie, currentMenu) {
        var container = li.querySelector('.movie-item-other-info');
        switch (currentMenu) {
            case 'menu-whats-new':
                container.innerHTML = movie.file.created.toDateString();
                break;
            case 'menu-safe-for-kids':
                if (movie.info.rated) {
                    container.innerHTML = movie.info.rated;
                }
                break;
            case 'menu-latest-releases':
                if (movie.info.released) {
                    container.innerHTML = movie.info.released.toDateString();
                }
                break;
            default:
                new jayhoo.RatingView(container, movie.info);
        }
    },
    showLoadingBackground: function(id) {
        var li = this._containerElement.querySelector('#' + id);
        if (li) {
            li.querySelector('.movie-poster').className = 'movie-poster loader';
        }
    },
    getPreviousMovieId: function(id) {
        var li = this._containerElement.querySelector('#' + id).previousSibling;
        return li ? li.id : null;
    },
    getNextMovieId: function(id) {
        var li = this._containerElement.querySelector('#' + id).nextSibling;
        return li ? li.id : null;
    }
};
