// let's declare our namespace with initial properties
window.jayhoo = {
    // this will be used to populate the window title of the application
    title: 'jayhoo! movie collection viewer',
    // collection of paths to be used by the application
    paths: {
        // this path is going to be used as the base path of all relative paths
        // that will be added to this object. Running the application as HTA is
        // different from running it from the browser. When running it as HTA,
        // the base path of the application is the folder path where the HTA file
        // is located however, running it from the browser, the base path is
        // the USER's desktop that is why we set it correctly to the path of the folder
        // containing the HTA file. Please take note of the forward slash (or double
        // backslash if that's what you used) at the end. window.external is NOT
        // supported by HTA so that's our hint to determine if we are running as an
        // HTA app or not
        application: !window.external ? '' : 'C:/Users/redcat/Documents/projects/jayhoo/',
        /**
         * Adds a paths property
         * @param {String} name         name of the property
         * @param {String} relativePath The relative path
         */
        add: function(name, relativePath) {
            this[name] = this.application + relativePath;
        }
    }
};

/**
 * This is the main application controller, it bootstraps everything
 * and show the application in a GOOD state
 */
jayhoo.AppController = function() {

    // the default regex object to match title and year
    // (eg Iron Man 3 2013 or Iron Man 3 (2013))
    // this is also the regex object to be used for manual
    // updating... the regex in the INI file is only used
    // for scanning the file system
    this._rgxTitleYear = /^(.+)[ ][\(]?(\d{4})[\)]?$/i;

    // initialize the file system object and ensure
    // that the data and folder logs exist
    this._fileSystem = new jayhoo.WinFileSystem();
    this._fileSystem.ensureFolder(jayhoo.paths.data);
    this._fileSystem.ensureFolder(jayhoo.paths.log);

    // initialize our file logger
    jayhoo.logger.init(jayhoo.paths.log, this._fileSystem);

    // our movie repository needs the file system for this case
    // and the path
    this._movieRepo = new jayhoo.MovieFileSystemRepository(this._fileSystem, jayhoo.paths.data);
    this._movieMetaProvider = new jayhoo.OmdbMovieMetaProvider();
    this._pagination = new jayhoo.Pagination();

    // read the INI file and initialize as an option object (hash)
    // convert necessary data types like INT and REGEXP
    this._options = this._fileSystem.readIniFile(jayhoo.paths.ini);
    this._options.extensions = this._options.extensions.split('|');
    this._options.safe_for_kids = this._options.safe_for_kids.split('|');
    this._options.pagination = parseInt(this._options.pagination);
    if (this._options.rgx_title_year) {
        this._options.rgx_title_year = new RegExp(this._options.rgx_title_year, 'i');
        this._options.rgx_title_index = parseInt(this._options.rgx_title_index);
        this._options.rgx_year_index = parseInt(this._options.rgx_year_index);
    } else {
        this._options.rgx_title_year = this._rgxTitleYear;
        this._options.rgx_title_index = 1;
        this._options.rgx_year_index = 2;
    }

    if (this._options.path === '') {
        throw new Error('Movie path is not set in the INI file.');
    }

    // if a language file is set, we will read it as well
    // and execute it (no, eval is not that evil :) )
    var languageFile = jayhoo.paths.language + this._options.language + '.js';
    if (this._fileSystem.fileExists(languageFile)) {
        eval(this._fileSystem.readTextFile(languageFile));
    }

    // initialize view objects
    this._pageView = new jayhoo.PageView(this._options.app_header);
    this._movieListView = new jayhoo.MovieListView();
    this._menuView = new jayhoo.MenuView();
    this._paginationView = new jayhoo.PaginationView();
    this._lightboxView = new jayhoo.LightBoxView();
    this._searchBoxView = new jayhoo.SearchBoxView();
    this._footerView = new jayhoo.FooterView();

    // initialize our app controller properties
    this._movies = [];
    this._currentMenu = '';
    this.attachEventHandlers();
};

jayhoo.AppController.prototype = {
    attachEventHandlers: function() {
        this.attachMenuEventHandlers();
        this.attachPaginationEventHandlers();
        this.attachMovieMetaProviderEventHandlers();
        this.attachMovieListViewEventHandlers();
        this.attachSearchEventHandlers();
        this.attachFooterActionEventHandlers();
        this.attachLightBoxEventHandlers();
    },
    attachMenuEventHandlers: function() {
        // what shall we do when a menu item is clicked
        this._menuView.menuItemSelectedEvent.attach(function(args) {
            this._currentMenu = args.menu;
            switch (args.menu) {
                case 'menu-all':             this.displayAllMovies(); break;
                case 'menu-whats-new':       this.displayWhatsNewMovies(); break;
                case 'menu-safe-for-kids':   this.displaySafeForKidsMovies(); break;
                case 'menu-latest-releases': this.displayLatestReleaseMovies(); break;
                case 'menu-top-rated':       this.displayTopRatedMovies(); break;
            }
            // clear the search box everytime a menu is selected
            this._searchBoxView.clear();
        }, this);
    },
    attachPaginationEventHandlers: function() {
        // what we do when the user clicked on our pagination view
        // eg the previous and next arrows
        this._paginationView.pageClickedEvent.attach(function(args) {
            this._pagination.setCurrentPage(args.page);
        }, this);
        // what we do when the actual pagination object changes a page
        this._pagination.pageChangedEvent.attach(function(args) {
            // make sure we sync our view from our object
            this._paginationView.update(this._pagination);
            // display only movies that are covered by our pagination object
            this.displayMovies(args.startOffset, args.endOffset);
        }, this);
    },
    attachMovieMetaProviderEventHandlers: function() {
        // what we do when a movie item is looked up in a database or web (for this instance)
        this._movieMetaProvider.beforeGetMovieMetaEvent.attach(function(args) {
            // we show a loader image just to let the user know
            this._movieListView.showLoadingBackground(args.movie.id);
            // we also logged it just to let the programmer know LOL!
            jayhoo.logger.info('Downloading movie info: ', args.params);
        }, this);
        // what we do once the look up is completed
        this._movieMetaProvider.afterGetMovieMetaEvent.attach(function(args) {
            if (args.success) {
                // update the repo and log the result
                this._movieRepo.update(args.movie);
                jayhoo.logger.info('Download success: ', args.movie.info.title);
            } else {
                jayhoo.logger.info('Download failed: ', args.error);
            }
            // sync the update to our view (movie list view)
            this._movieListView.updateMovieListItem(args.movie);
        }, this);
    },
    attachMovieListViewEventHandlers: function() {
        // what we do when the user clicks the play button
        this._movieListView.playMovieClickedEvent.attach(function(args) {
            // executing a path actually calls the default application
            // to handle this file by file extension
            this._fileSystem.exec(this._movieRepo.get(args.movieId).file.path);
        }, this);
        // what we do when the user clicks on a movie item menu
        this._movieListView.movieItemMenuClickedEvent.attach(function(args) {
            var movie = this._movieRepo.get(args.movieId);
            switch (args.menu) {
                case 'movie-item-menu-more-info':
                    this._lightboxView.show(movie);
                    break;
                case 'movie-item-menu-web-info':
                    if (movie.info.externalId) {
                        this._fileSystem.exec(movie.info.url);
                    }
                    break;
                case 'movie-item-menu-update-info':
                    this.updateMovieInfo(movie);
                    break;
                case 'movie-item-menu-update-poster':
                    this.updateMoviePoster(movie);
                    break;
                case 'movie-item-menu-open-folder':
                    this._fileSystem.openFolder(movie.file.folderPath);
                    break;
            }
        }, this);
    },
    attachSearchEventHandlers: function() {
        // what we do when the user inputs a search text
        // and clicks search or typed ENTER
        this._searchBoxView.searchEvent.attach(function(args) {
            if (args.value) {
                return this.displaySearchedMovies(args.value);
            }
            return '';
        }, this);
    },
    attachFooterActionEventHandlers: function() {
        // what we do when the user clicks the clean data files link
        this._footerView.cleanOrphanedDataClickedEvent.attach(function() {
            if (jayhoo.dialog.confirm(_('Please confirm deletion of data files with no (more) corresponding movie file.'))) {
                var deleted = this._movieRepo.deleteOrphanedDataFiles();
                jayhoo.dialog.alert(_('%s file(s) have been deleted.', deleted));
            }
        }, this);
        // what we do when the user clicks any other links from the footer
        this._footerView.outerLinksClickedEvent.attach(function(args) {
            this._fileSystem.exec(args.url);
        }, this);
    },
    attachLightBoxEventHandlers: function() {
        // what we do when the user wanted the lightbox to show the next movie
        // either by pressing the RIGHT ARROW key or clicking the NEXT link
        this._lightboxView.onShowNextMovieEvent.attach(function(args) {
            // get the next DISPLAYED movie
            var nextMovieId = this._movieListView.getNextMovieId(args.movieId);
            if (nextMovieId) {
                // if there is, then show the lightbox with this movie
                this._lightboxView.show(this._movieRepo.get(nextMovieId));
            }
        }, this);
        // what we do when the user wanted the lightbox to show the next movie
        // either by pressing the LEFT ARROW key or clicking the PREVIOUS link
        this._lightboxView.onShowPreviousMovieEvent.attach(function(args) {
            // get the previous DISPLAYED movie
            var previousMovieId = this._movieListView.getPreviousMovieId(args.movieId);
            if (previousMovieId) {
                // if there is, then show the lightbox with this movie
                this._lightboxView.show(this._movieRepo.get(previousMovieId));
            }
        }, this);
    },
    updateMoviePoster: function(movie) {
        // prompt the user for URL
        var url = jayhoo.dialog.prompt(_('Input the image URL:'));
        if (isValidUrl(url)) {
            movie.info.poster = url;
            this._movieListView.updateMovieListItem(movie);
            this._movieRepo.update(movie);
        } else if (url) {
            jayhoo.dialog.alert(_('Invalid URL'));
        }
    },
    updateMovieInfo: function(movie) {
        // prompt for movie title/year or external ID
        var value = jayhoo.dialog.prompt(_('Input movie title and year or the external ID:'));
        if (!value) {
            return;
        }
        if (this._movieMetaProvider.isValidExternalId(value)) {
            this._movieMetaProvider.updateMovieInfo(movie, { externalId: value.trim() });
        } else {
            // else, this must be a title with year
            var match = this._rgxTitleYear.exec(value);
            if (match) {
                this._movieMetaProvider.updateMovieInfo(movie, {
                    title: match[1],
                    year: match[2]
                });
            } else {
                jayhoo.dialog.alert(_('Invalid input. Please try again.'));
            }
        }
    },
    /**
     * displays all the movies without any filter and sorting
     * the number of movies shown depends upon the pagination
     */
    displayAllMovies: function() {
        this._movies = this._movieRepo.getAll();
        this.resetPagination();
    },
    /**
     * display all the movies sorted by the date downloaded/created
     */
    displayWhatsNewMovies: function() {
        this._movies = this._movieRepo.getAll(false, [
            { type: '',     field: 'year',    order: 'asc' },
            { type: 'file', field: 'created', order: 'des' }
            ]);
        this.resetPagination();
    },
    /**
     * display all the movies that passes the filter of safe_for_kids INI setting
     */
    displaySafeForKidsMovies: function() {
        var filterOptions = [];
        // first, let's gather all the rateds in an array
        for (var i in this._options.safe_for_kids) {
            filterOptions.push({
                field: 'rated',
                value: this._options.safe_for_kids[i],
                exact: true
            });
        }
        // then pass that array to the getAll method,
        // we will also sort it by year and title
        this._movies = this._movieRepo.getAll(filterOptions, [
            { field: 'year',  order: 'asc' },
            { field: 'title', order: 'asc' }
            ]);
        this.resetPagination();
    },
    /**
     * display all movies sorted according to the date it was released
     */
    displayLatestReleaseMovies: function() {
        this._movies = this._movieRepo.getAll([], [{
            type:  'info',
            field: 'released',
            order: 'des'
        }]);
        this.resetPagination();
    },
    /**
     * display all movies sorted by rating in descending order
     */
    displayTopRatedMovies: function() {
        this._movies = this._movieRepo.getAll([], [{
            type:  'info',
            field: 'rating',
            order: 'des'
        }]);
        this.resetPagination();
    },
    /**
     * display all movies that qualifies our search text
     * @param  {String} searchText the search text (can be an advanced search text)
     */
    displaySearchedMovies: function(searchText) {
        var filterOptions = [];
        var hasMatch = false;
        var validFieldsForSearch = [ 'title', 'year', 'genres', 'actors', 'directors', 'rated' ];

        // let's check if the search text has the advanced search format (URL QUERY-LIKE)
        searchText.replace(/([^?=&]+)(=([^&]*))?/g, function($0, $1, $2, $3) {
            if ($3 != undefined && validFieldsForSearch.indexOf($1.toLowerCase()) !== -1) {
                hasMatch = true;
                filterOptions.push({
                    field: $1,
                    value: $3,
                    exact: false
                });
            }
        });
        // if not, then we default to search by title
        if (!hasMatch) {
            filterOptions.push({
                field: 'title',
                value: searchText,
                exact: false
            });
        }
        // let's log it for the programmer to see,
        // maybe the user did not get what he/she wanted
        jayhoo.logger.info('Search filter: ', filterOptions);

        // get all the movies for this search
        // and sort by year and title
        this._movies = this._movieRepo.getAll(filterOptions, [
            { field: 'year',  order: 'asc' },
            { field: 'title', order: 'asc' }
            ], true);

        // clear the menu since we are not under any
        this._menuView.clearSelection();
        this.resetPagination();

        // alert the user the number of movies found
        jayhoo.dialog.alert(_('%s movie(s) found.', this._movies.length));

        // let's show the user how his search string was transformed
        var validSearchString = [];
        for (var i in filterOptions) {
            validSearchString.push(filterOptions[i].field + '=' + filterOptions[i].value);
        }
        return validSearchString.join('&');
    },
    resetPagination: function() {
        this._pagination.reset(this._options.pagination, this._movies.length);
        this._pagination.setCurrentPage(1);
    },
    /**
     * displays only the movies covered by start and end
     * @param  {Number} start The offset key where we start
     * @param  {Number} end   The offset key where we end
     */
    displayMovies: function(start, end) {
        // adding movies to the view can be quite slow
        // luckily, the view provides us a way to do it
        // efficiently and fast
        this._movieListView.beginAddMovies();

        if (this._movies.length) {
            while (start <= end) {
                var movie = this._movies[start];
                this._movieListView.addMovie(movie, this._currentMenu);
                if (!movie.info.externalId) {
                    this._movieMetaProvider.updateMovieInfo(movie, {
                        title: movie.info.title,
                        year: movie.info.year
                    });
                }
                start++;
            }
        }

        this._movieListView.endAddMovies();
    },
    scanFileSystem: function() {
        this._fileSystem.scanFolder(this._options.path, this.addMovieFile.bind(this));
        this._menuView.selectDefault();

        if (this._movies.length === 0) {
            jayhoo.dialog.alert(_('There are no movie files found.'));
        }
    },
    addMovieFile: function(fileInfo) {
        if (this._options.extensions.indexOf(fileInfo.extension) !== -1) {
            var match = this._options.rgx_title_year.exec(fileInfo.folder);
            if (match) {
                this._movieRepo.add({
                    file: fileInfo,
                    info: {
                        title: match[this._options.rgx_title_index],
                        year : parseInt(match[this._options.rgx_year_index]),
                    }
                });
                return true;
            }
        }
        return false;
    }
};

window.onload = function() {

    try {

        jayhoo.paths.add('ini', 'jayhoo.ini');
        jayhoo.paths.add('data', 'data/');
        jayhoo.paths.add('log', 'logs/');
        jayhoo.paths.add('language', 'i18n/');

        var splash = showSplashScreenWindow();
        var controller = new jayhoo.AppController();
        controller.scanFileSystem();
        splash.close();
    } catch (err) {
        jayhoo.dialog.alert(_('Fatal error encountered:\n\n%s\n\n%s', err.message, err.stack));
        jayhoo.logger.error('Application failed!', err.message, err.stack);
        window.close();
        if (splash) {
            splash.close();
        }
    }
};

function showSplashScreenWindow() {
    var width = 500;
    var height = 200;
    var left = (screen.width / 2) - (width / 2);
    var top = (screen.height / 2) - (height / 2);
    return window.open('res/splash.html', 'splash', 'width=' + width
        + ',height=' + height + ',top=' + top + ',left=' + left
        + ',directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0,scrollbars=0');
}